struct TBinStr{
	char *s[3];

	void clear(){
		for (int i = 0; i < 3; i++)
			delete[] s[i];
	}
};

THuffman_Tree H_tr;

void menu(){
	cout << "Choose:" << endl;
	cout << " 0) Close;" << endl;
	cout << " 1) " << '"' << "beep boop beer!" << '"' << ';' << endl;
	cout << " 2) Enter string;" << endl;
	cout << " 3) Using file;" << endl;
}

void bin_show(const string &s){
	int mask = 0;
	int bit = 0;
	for (int i = 0; i < s.length(); i++){
		mask = 128;
		for (int j = 0; j < 8; j++){
			bit = s[i] & mask;
			bit >>= 7 - j;
			mask >>= 1;
			cout << bit;
		}
	}
	cout << endl;
}

void show_res(string *s){
	cout << "In string form:" << endl;
	cout << " First string:" << endl;
	cout << s[0] << endl << endl;
	cout << " Second string:" << endl;
	cout << s[1] << endl << endl;
	cout << " Third string:" << endl;
	cout << s[2] << endl << endl;
	cout << endl;
	cout << "In binary form:" << endl;
	cout << " First string:" << endl;
	bin_show(s[0]);
	cout << endl;
	cout << " Second string:" << endl;
	bin_show(s[1]);
	cout << endl;
	cout << " Third string:" << endl;
	bin_show(s[2]);
	cout << endl;
}

void w_string(const string &s){
	string *res_cod = 0;
	res_cod = H_tr.Coded(s);
	cout << "Source string:" << endl;
	cout << s << endl;
	cout << endl;
	cout << "Result of compression:" << endl;
	if (res_cod[0].length())
		show_res(res_cod);
	else
		cout << res_cod[2] << endl;
	cout << endl;
	string res_rem;
	res_rem = H_tr.Recover(res_cod);
	cout << "Result of recover:" << endl;
	cout << res_rem << endl;
	cout << endl;
	delete[] res_cod;
}

int coded_file(){
	ifstream orig("Original.txt");
	if (orig){
		ofstream res_Cfile("Comression_file.txt", ios::out | ios::binary | ios::trunc);
		string s;
		string *res_Cstr = 0;
		TBinStr str;
		while (!orig.eof()){
			getline(orig, s);
			res_Cstr = H_tr.Coded(s);
			for (int i = 0; i < 3; i++){
				int l = res_Cstr[i].size();
				str.s[i] = new char[l];
				for (int j = 0; j < l; j++)
					str.s[i][j] = res_Cstr[i][j];
				str.s[i][l] = '\0';
			}
			res_Cfile.write((char *)&str, sizeof str);
			delete[] res_Cstr;
			s.clear();
		}
		return 1;
	}
	return 0;
}

void recover_file(){
	ofstream res_Rfile("Remove_file.txt", ios::out | ios::trunc);
	ifstream res_Cfile("Comression_file.txt", ios::binary);
	string *res_Cstr = new string[3];
	string res_Rstr;
	while (!res_Cfile.eof()){
		TBinStr str;
		res_Cfile.read((char *)&str, sizeof str);
		if (!res_Cfile.eof()){
			for (int i = 0; i < 3; i++){
				res_Cstr[i] += str.s[i];
			}
			res_Rstr = H_tr.Recover(res_Cstr);
			for (int i = 0; i < 3; i++)
				res_Cstr[i].clear();
			res_Rfile << res_Rstr << endl;
			res_Rstr.clear();
		}
	}
}