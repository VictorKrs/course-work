#include <vector>
#include "TRoot.h"

using namespace std;

//	node structure of queue with priority
struct Q_Node{
public:
	TRoot *Node;
	int P;

	Q_Node(){ Node = 0; P = 0; }
	Q_Node(const Q_Node &Q);
};

Q_Node::Q_Node(const Q_Node &Q){
	P = Q.P;
	if (Q.Node)
		Node = new TRoot(Q.Node);
}

//	queue with priority
class TQeue{
protected:
// Fields
	vector<Q_Node> C;			//	Container

public:
//Methods
	void push(Q_Node n);
	Q_Node top();
	void pop();
	int empty();
	void clear() { C.clear(); }
	int size() { return C.size(); }
};

void TQeue::push(Q_Node n){
	C.push_back(n);
	int i = C.size() - 1;
	while (i > 0 && C[i - 1].P < C[i].P){
		Q_Node temp = C[i];
		C[i] = C[i - 1];
		i--;
		C[i] = temp;
	}
}

Q_Node TQeue::top(){
	return C[C.size() - 1];
}

void TQeue::pop(){
	C.pop_back();
}

int TQeue::empty(){
	return !C.size();
}