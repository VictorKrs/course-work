// Coursework.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include "THuffman_Tree.h"
#include "Function.cpp"


int _tmain(int argc, _TCHAR* argv[])
{
	int choose = 1;
	cout << "===============================================================================" << endl;
	cout << "                      Demonstration of ATD Huffman tree" << endl;
	cout << "                Fulfilled: Krasniy Victor Andreevich, IVB-2-13" << endl;
	cout << "===============================================================================" << endl;
	while (choose){
		menu();
		cin >> choose;
		switch (choose){
			case 1:{
				string s("beep boop beer!");
				w_string(s);
			}
				break;
			case 2:{
				string s;
				cout << "Enter string: ";
				cin.ignore(cin.rdbuf()->in_avail());
				getline(cin, s);
				w_string(s);
			}
				break;
			case 3:
				if (coded_file()){
					recover_file();
					cout << endl << "Completed!" << endl << endl;
				}
				else
					cout << endl << "Error: file don't oppened!" << endl << endl;
				break;
		}
	}
	return 0;
}