#include "TQeue.h"
#include <map>
#include <string>

typedef TQeue TPQ;
typedef map<char, string> H_arr;

class THuffman_Tree{
protected:
	// Fields
	TPQ Q;
	TRoot *T;
	bool flag;		//	True - need compress
	H_arr arr;

public:
	// Constructor & destructor
	THuffman_Tree(){ T = 0; flag = true; }
	~THuffman_Tree(){ 
		if (T) 
		T->clear(T); }

	// Methods
	string* Coded(const string &s);		//	coded string
	string Recover(string *s);			//	recover string

protected:
	void fill_Q(const string &s);		//	create queue
	void tree_H();		//	create Huffman tree
	void form_arrH(TRoot *R, string &s);		//	create Huffman array
	void coding_Tree(TRoot *R, string *s, int &count);	//	coding Huffman tree in string
	string coding_str(const string &s);			//	coding input string
	string* coded(const string &s);		//	coding Huffman tree and input string	
	void rec_tree(TRoot *&R, string *s, int &i0, int &i1, int &mask);	//	reconstruction of huffman tree
	char dec_ch(string &s, int &ind, int &mask);		//	reconstruction of symbol
	string decoding(string *s);			//	reconstruction of string
	void rec_last_s(string &s, string &res, int mask);	// reconstruction of last elements
};

string* THuffman_Tree::Coded(const string &s){
	// s - input string
	if (T){				// clearing all fields;
		T->clear(T);
		T = 0;
		arr.clear();
		Q.clear();
	}
	fill_Q(s);
	if (flag){
		tree_H();
		string s1;
		form_arrH(T, s1);
		return coded(s);
	}
	string *res = new string[3];
	res[2] += s;
	return res;
}

string THuffman_Tree::Recover(string *s){
	// *s - array of encoded string
	if (T){				// clearing all fields;
		T->clear(T);
		T = 0;
		arr.clear();
		Q.clear();
	}
	if (s[0].length())
		return decoding(s);
	return s[2];
}

void THuffman_Tree::fill_Q(const string &s){
	//	s - input string
	map<char, int> m;
	for (int i = 0; i < s.length(); i++)
		m[s[i]]++;
	flag = (double(m.size()) / s.length()) < (double(2) / 3);
	if (flag){
		Q_Node elem;
		elem.Node = new TRoot;
		for (auto i : m){
			elem.P = i.second;
			elem.Node->ch = i.first;
			Q.push(elem);
		}
	}
}

void THuffman_Tree::tree_H(){
	Q_Node N1, N2, NRes;
	NRes.Node = new TRoot;
	if (Q.size() == 1)
		NRes = Q.top(), Q.pop();
	while (!Q.empty()){
		N1 = Q.top();
		Q.pop();
		if (!Q.empty()){
			N2 = Q.top();
			Q.pop();
			NRes.P = N1.P + N2.P;
			NRes.Node->Left = N1.Node;
			NRes.Node->Right = N2.Node;
			Q.push(NRes);
		}
	}
	T = NRes.Node;
}

void THuffman_Tree::form_arrH(TRoot *R, string &s){
	//	s - need for forming codes of symbols
	if (!R->Left && !R->Right){
		if (!s.size()) s += 1;
		arr[R->ch] = s;
	}
	else{
		s.push_back(0), form_arrH(R->Left, s), s.pop_back();			//	Left brother
		s += 1, form_arrH(R->Right, s), s.pop_back();					//	Right brother
	}
}

void THuffman_Tree::coding_Tree(TRoot *R, string *s, int &count){
	// *s - array of output strings
	// count - number of shifts
	if (count == 8)
		s[0].push_back(1), count = 0;
	int i = s[0].size() - 1;
	// filling
	if (!R->Left){					//	if sheet
		s[0][i] <<= 1;
		count++;
		s[0][i] += 1;
		s[1] += R->ch;
	}
	else{							//	if node
		s[0][i] <<= 1;
		count++;
		coding_Tree(R->Left, s, count);
		coding_Tree(R->Right, s, count);
	}
}

string THuffman_Tree::coding_str(const string &s){
	// s - input string
	string res;		//	result
	string bits;	//	new code of symbol
	int is = 0;			//	index of input string
	int count = 8;		//	number of shifts
	int ir = -1;		//	index of result string
	while (is < s.size()){
		bits = arr[s[is]];
		is++;
		int j = 0;
		//	Bitwise filling string of result
		while (j < bits.size()){
			if (count == 8)
				res.push_back(1), count = 0, ir++;
			res[ir] <<= 1;
			res[ir] += bits[j];
			j++;
			count++;
		}
	}
	res[ir] <<= 8 - count;
	res.push_back(count);
	return res;
}

string* THuffman_Tree::coded(const string &s){
	// s - input string
	string *res = new string[3];
	int count = 8;			//	number of shifts
	coding_Tree(T, res, count);		//	coding tree
	int i = res[0].size() - 1;		//	index of last element
	res[0][i] <<= 8 - count;		//	shiht of last element
	res[2] = coding_str(s);			//	coding string
	return res;
}

void THuffman_Tree::rec_tree(TRoot *&R, string *s, int &i0, int &i1, int &mask){
	// R - node of recovered tree
	// *s - array of output strings
	// i0 - index of string s[0]
	// i1 - index of string s[1]
	if (!mask)
		mask = 128, i0++;
	R = new TRoot;
	int x = s[0][i0] & mask;	//	getting bit
	mask >>= 1;
	if (x)
		R->ch = s[1][i1], i1++;
	else{
		rec_tree(R->Left, s, i0, i1, mask);
		rec_tree(R->Right, s, i0, i1, mask);
	}
}

char THuffman_Tree::dec_ch(string &s, int &ind, int &mask){
	// s - encoded string
	TRoot *R = T;
	int bit;
	while (R->Left){
		if (!mask)
			ind++, mask = 128;
		bit = s[ind] & mask;
		mask >>= 1;
		if (bit)
			R = R->Right;
		else
			R = R->Left;
	}
	return R->ch;
}

string THuffman_Tree::decoding(string *s){
	// *s - array of output strings
	//	reconstruction of tree
	int i1 = 0, i2 = 0;
	int mask = 128;
	rec_tree(T, s, i1, i2, mask);

	//	reconstruction of string
	string res;
	if (!T->Left){			//	if one symbol in input string
		int il = s[2].size() - 1;
		for (int i = 0; i < (il - 1) * 8 + s[2][il]; i++)
			res += s[1][0];
		return res;
	}
	i2 = 0; mask = 128;
	int il = s[2].size() - 1;		//	index of last element
	while (i2 < il - 1)
		res += dec_ch(s[2], i2, mask);

	// reconstruction of last elements
	if (!mask && i2 != il - 1)
		mask = 128;
	rec_last_s(s[2], res, mask);
	return res;
}

void THuffman_Tree::rec_last_s(string &s, string &res, int mask){
	// s - ensoded string
	// res - result string
	int il = s.size() - 1;
	int count = s[il];
	int mask1 = 128;
	mask1 >>= count;	//	reference mask
	TRoot *R = T;
	int bit;
	while (mask > mask1){
		bit = s[il - 1] & mask;
		mask >>= 1;
		if (bit)
			R = R->Right;
		else
			R = R->Left;			
		if (!R->Left)
			res += R->ch, R = T;
	}
}