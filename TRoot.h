//	node structure of Huffman tree
class TRoot{
public:
	char ch;
	TRoot *Left, *Right;
	TRoot(){ ch = 0; Left = 0; Right = 0; }
	TRoot(char a){ ch = a; Left = 0; Right = 0; }
	TRoot(const TRoot *R);
	void clear(TRoot *&R);
};

TRoot::TRoot(const TRoot *R){
	ch = R->ch;
	if (R->Left)
		Left = new TRoot(R->Left);
	else
		Left = 0;
	if (R->Right)
		Right = new TRoot(R->Right);
	else
		Right = 0;
}

void TRoot::clear(TRoot *&R){
	if (R->Left){
		clear(R->Left);
		clear(R->Right);
	}
	delete R;
}